// Import libraries.
var express = require('express')
	, path = require('path')
	, favicon = require('serve-favicon')
	, logger = require('morgan')
	, cookieParser = require('cookie-parser')
	, bodyParser = require('body-parser')
	, mongoose = require('mongoose');

mongoose.Promise = global.Promise;

// Variables
var app = express()
	, port = process.env.PORT || 5001
	// MongoDB Configuration
	, configDB = require('./config/config.js');

mongoose.connect(configDB.uri, configDB.options)
	.then(() =>  console.log('connection succesful'))
	.catch((err) => console.error(err));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Set up our express application.
app.use(logger('dev'));
app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'bower_components')));
app.set('views', path.join(__dirname, '/app/views'));
app.set('view engine', 'jade');

//routes ======================================================================
var index = require('./routes/index')
	, members = require('./routes/members');

app.use('/', index);
app.use('/members', members);

//launch ======================================================================
app.listen(port);
console.log('The magic happens on port ' + port);

//catch 404 and forward to error handler
app.use(function (req, res, next) {
    res.status(404).render('404', {
    	title: "404", 
		type: "404", 
		message: "Sorry, an error has occured, Requested page not found!", 
		session: req.sessionbo});
});

app.use(function (req, res, next) {
    res.status(500).render('404', {
    	title: "500",
		type: "500",
    	title: "Sorry, page not found"});
});

exports = module.exports = app;
