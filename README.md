# README #

Table of Contents
-----------------

- [Features](#features)
- [Prerequisites](#prerequisites)
- [Getting Started](#getting-started)
- [Project Structure](#project-structure)
- [List of Packages](#list-of-packages)

Features
--------

This application is a CRUD for the management of members of a musical band.

- Create Members (/members/create)
- Read list of members (/members)
- Update members (/members/edit/:id)
- Delete members (/members/delete/:id)
- MVC Project Structure

Prerequisites
-------------

- [MongoDB](https://www.mongodb.org/downloads)
- [Node.js 6.0+](http://nodejs.org)

Getting Started
---------------

# Install NPM dependencies
npm install

# Install Bower dependencies
npm install -g bower
bower install

Project Structure
-----------------

| Name                                 | Description                                                  |
| ------------------------------------ | ------------------------------------------------------------ |
| **config**/config.js                 | MongoDB configurations                                       |
| **config**/constants.js              | Constants file for application                               |
| **config**/routes.js                 | File with defined routes                                     |
| **app/controllers**/member.js        | Controller for member management.                            |
| **app/models**/members.js            | Mongoose schema and model for Member.                        |
| **app/views/members**/               | Templates for *create, edit, show, index*.                   |
| **app/views/404.jade**/              | 404 page Template.                                           |
| **app/views/home.jade**/             | Home page template.                                          |
| **app/views/layout.jade**/           | Base template.                                               |
| **public**/                          | Static assets (fonts, css, js, img).                         |
| **public**/**javascripts**/          | Place your client-side JavaScript here.                      |
| **public**/**stylesheet**/style.css  | Main stylesheet for your app.                                |
| app.js                               | The main application file.                                   |
| package.json                         | NPM dependencies.                                            |
