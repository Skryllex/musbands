// MongoDB configurations.

module.exports = {
  uri: 'mongodb://localhost:27017/musbands',
  options: { useMongoClient: true }
}

