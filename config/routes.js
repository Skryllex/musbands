// Routes for app.

var express = require('express')
	, router = express.Router()
	, memberController = require('../app/controllers/member');

// Get all members.
router.get('/', function(req, res) {
	memberController.list(req, res);
});

// Get single member by id.
router.get('/show/:id', function(req, res) {
	memberController.show(req, res);
});

// Create member.
router.get('/create', function(req, res) {
	memberController.create(req, res);
});

// Save member.
router.post('/save', function(req, res) {
	memberController.save(req, res);
});

// Edit member.
router.get('/edit/:id', function(req, res) {
	memberController.edit(req, res);
});

// Edit member.
router.post('/update/:id', function(req, res) {
	memberController.update(req, res);
});

// Delete member.
router.post('/delete/:id', function(req, res, next) {
	memberController.delete(req, res);
});

module.exports = router;