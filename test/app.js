// App tests.

const request = require('supertest')
	, app = require('../app.js');

describe('GET /', () => {
  it('should return 200 OK', (done) => {
    request(app)
      .get('/')
      .expect(200, done);
  });
});

describe('GET /members', () => {
  it('should return 200 OK', (done) => {
    request(app)
      .get('/members')
      .expect(200, done);
  });
});

describe('GET /members/create', () => {
  it('should return 200 OK', (done) => {
    request(app)
      .get('/members/create')
      .expect(200, done);
  });
});

describe('GET /random-url', () => {
  it('should return 404', (done) => {
    request(app)
      .get('/random-url')
      .expect(404, done);
  });
});
