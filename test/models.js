// Model tests.

const mongoose = require('mongoose')
	, {expect} = require('chai')
	, sinon = require('sinon');
require('sinon-mongoose');

const MemberModel = require('../app/models/members');

describe('Member Model', () => {
  it('should create a new member', (done) => {
    const MemberMock = sinon.mock(new MemberModel({ 
    	name: 'Jona',
		surname: 'Weinhofen',
		age: 32,
		function: 'rythmic guitar',
		website: 'https://es.wikipedia.org/wiki/Bring_Me_the_Horizon',
		states: 'inactive'
		
    }));
    
    const member = MemberMock.object;

    MemberMock
      .expects('save')
      .yields(null);

    member.save(function (err, result) {
    	MemberMock.verify();
    	MemberMock.restore();
    	expect(err).to.be.null;
    	done();
    });
  });

  it('should return error if member is not created', (done) => {
    const MemberMock = sinon.mock(new MemberModel({ 
    	name: 'Jona',
		surname: 'Weinhofen',
		age: 32,
		function: 'rythmic guitar',
		website: 'https://es.wikipedia.org/wiki/Bring_Me_the_Horizon',
		states: 'inactive'
    }));
    const member = MemberMock.object;
    const expectedError = {
      name: 'ValidationError'
    };

    MemberMock
      .expects('save')
      .yields(expectedError);

    member.save((err, result) => {
    	MemberMock.verify();
    	MemberMock.restore();
    	expect(err.name).to.equal('ValidationError');
    	expect(result).to.be.undefined;
    	done();
    });
  });

  it('should find member by name', (done) => {
    const MemberMock = sinon.mock(MemberModel);
    const expectedUser = {
      name: 'Jona',
      surname: 'Weinhofen'
    };

    MemberMock
      .expects('findOne')
      .withArgs({ name: 'Jona' })
      .yields(null, expectedUser);

    MemberModel.findOne({ name: 'Jona' }, (err, result) => {
    	MemberMock.verify();
    	MemberMock.restore();
    	expect(result.name).to.equal('Jona');
    	done();
    })
  });

  it('should remove member by name', (done) => {
    const MemberMock = sinon.mock(MemberModel);
    const expectedResult = {
      nRemoved: 1
    };

    MemberMock
      .expects('remove')
      .withArgs({ name: 'Jona' })
      .yields(null, expectedResult);

    MemberModel.remove({ name: 'Jona' }, (err, result) => {
    	MemberMock.verify();
    	MemberMock.restore();
    	expect(err).to.be.null;
    	expect(result.nRemoved).to.equal(1);
    	done();
    })
  });
});
