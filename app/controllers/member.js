// Controller for model Member.

var mongoose = require('mongoose')
	, MemberModel = require('../models/members.js');

var memberController = {};

var functions = {
	'bass': ['Bass'],
	'lead guitar': ['Lead guitar'],
	'drum': ['Drum'],
	'lead vocal': ['Lead vocal'],
	'rythmic guitar': ['Rhythmic guitar']
};

var states = {
	'active': ['Active'],
	'inactive': ['Inactive']
};


// Show list of members of band
memberController.list = function(req, res) {
	MemberModel.find({}).exec(function (err, members) {
		if (err) {
			console.log("Error:", err);
		}
		else {
			res.render("../views/members/index", {
				title: 'Members',
				members: members
			});
		}
	});
};

// Show member by id
memberController.show = function(req, res) {
	MemberModel.findOne({_id: req.params.id}).exec(function (err, member) {
		if (err) {
			console.log("Error:", err);
		}
		else {
			console.log(member);
			res.render("../views/members/show", {
				title: 'Show',
				member: member
			});
		}
	});
};

// Create new member
memberController.create = function(req, res) {
	res.render("../views/members/create", {
		title: 'Create',
		functions: functions,
		states: states
	});
};

// Save new member
memberController.save = function(req, res) {
  var member = new MemberModel(req.body);

  member.save(function(err) {
    if(err) {
      console.log(err);
      res.render("../views/members/create", {
    	  title: 'Create',
		  error: err,
		  functions: functions,
		  states: states
      });
    } else {
      console.log("Successfully created a member.");
      res.redirect("/members/show/" + member._id);
    }
  });
};

// Edit an member
memberController.edit = function(req, res) {
	MemberModel.findOne({_id: req.params.id}).exec(function (err, member) {
	    if (err) {
	      console.log("Error:", err);
	    }
	    else {
	      res.render("../views/members/edit", {
	    	  title: 'Edit',
	    	  functions: functions,
			  states: states,
	    	  member: member
		  });
	    }
  });
};

// Update a member
memberController.update = function(req, res) {
	MemberModel.findByIdAndUpdate(req.params.id, { 
		$set: { 
			name: req.body.name, 
			surname: req.body.surname, 
			age: req.body.age, 
			function: req.body.function,
			website: req.body.website,
			states: req.body.states}
		}, { new: true }, function (err, member) {
    if (err) {
      console.log(err);
      res.render("../views/members/edit", {
    	  title: 'Edit',
		  error: err,
    	  member: req.body
	  });
    }
    res.redirect("/members/show/" + member._id);
  });
};

// Delete a member
memberController.delete = function(req, res) {
  MemberModel.remove({_id: req.params.id}, function(err) {
    if(err) {
      console.log(err);
    }
    else {
      console.log("Member deleted!");
      res.redirect("/members");
    }
  });
};

module.exports = memberController;