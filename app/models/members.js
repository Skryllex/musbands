// Memeber model.

var mongoose = require('mongoose');

var MemberSchema = new mongoose.Schema({
	'name' : String,
	'surname' : String,
	'age' : Number,
	'function': String,
	'website' : String,
	'states': String,
	'updated_at' : {type: Date, default: Date.now }
});

module.exports = mongoose.model('Member', MemberSchema);